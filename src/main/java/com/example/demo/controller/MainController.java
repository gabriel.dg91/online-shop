package com.example.demo.controller;

import com.example.demo.dto.*;
import com.example.demo.model.Order;
import com.example.demo.service.*;
import com.example.demo.validator.ProductDtoValidator;
import com.example.demo.validator.UserDtoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Controller
public class MainController {
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductDtoValidator productDtoValidator;
    private RedirectAttributes redirectAttributes;
    @Autowired
    private ShoppingCartService shoppingCartService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserDtoValidator userDtoValidator;

    @Autowired
    private OrderService orderService;


    @GetMapping("/addProduct")
    public String productPageGet(Model model, @ModelAttribute("addProductMessage") String addProductMessage) {
        ProductDto productDto = new ProductDto();
        model.addAttribute("productDto", productDto);
        model.addAttribute("addProductMessage", addProductMessage);
        return "addProduct";
    }

    @PostMapping("/addProduct")
    public String productPagePost(@ModelAttribute(name = "productDto") ProductDto productDto, BindingResult bindingResult,
                                  RedirectAttributes redirectAttributes,
                                  @RequestParam("productImage") MultipartFile multipartFile) {

        productDtoValidator.validate(productDto, bindingResult);
        if (bindingResult.hasErrors()) {
            return "addProduct";
        }
        productService.addProduct(productDto, multipartFile);
        redirectAttributes.addFlashAttribute("addProductMessage", "The product was successfully added");
        return "redirect:/addProduct";
    }

    @GetMapping("/home")
    public String homePageGet(Model model) {
        List<ProductDto> productDtoList = productService.getAllAvailableProductDtos();
        model.addAttribute("productDtoList", productDtoList);
        return "home";
    }

    @GetMapping("/product/{productId}")
    public String viewPageGet(@PathVariable(value = "productId") String productId, Model model) {
        Optional<ProductDto> optionalProductDto = productService.findProductDtoById(productId);
        if (optionalProductDto.isEmpty()) {
            return "error";
        }
        model.addAttribute("productDto", optionalProductDto.get());
        model.addAttribute("quantityDto", new QuantityDto());
        return "viewProduct";
    }


    @GetMapping(value = "/register")
    public String registrationGet(Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("userDto", userDto);
        return "register";
    }

    @PostMapping(value = "/register")
    public String registrationPost(@ModelAttribute(name = "userDto") UserDto userDto,
                                   BindingResult bindingResult, Model model) {
        userDtoValidator.validate(userDto, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("message", "Invalid form");
            return "register";
        }
        boolean wasSuccessful = userService.addUser(userDto);
        if (wasSuccessful) {
            model.addAttribute("message", "User was successfully added!");
        }
        return "redirect:/login";
    }

    @GetMapping(value = "/login")
    public String loginGet(Model model) {
        LoginDto loginDto = new LoginDto();
        model.addAttribute("loginDto", loginDto);
        return "login";
    }

    @PostMapping(value = "/product/{productId}/add")
    public String addToCartPost(@PathVariable(value = "productId") String productId,
                                @ModelAttribute(name = "quantityDto") QuantityDto quantityDto,
                                Authentication authentication) {

        System.out.println("vreau sa adaug in cart produsul cu id-ul " + productId + "si cantitatea "
                + quantityDto.getQuantity() + " in cosul userului " + authentication.getName());
        shoppingCartService.addProductToShoppingCart(productId, quantityDto, authentication.getName());
        return "redirect:/product/" + productId;

    }

    @GetMapping(value = "/buyNow")
    public String buyNowGet(Authentication authentication, Model model){
        Map<ProductDto, QuantityPriceDto> productMap = shoppingCartService.retrieveShoppingCartContent(authentication.getName());
        model.addAttribute("productMap", productMap);

        PriceSummaryDto priceSummaryDto = shoppingCartService.computePriceSummaryDto(productMap);
        model.addAttribute("priceSummaryDto", priceSummaryDto);
        return "buyNow";
    }


    @GetMapping(value = "/confirmation")
    public String confirmationGet(Authentication authentication) {
        orderService.placeOrder((authentication.getName()));
        return "confirmation";
    }
    @GetMapping(value = "/orders")
    public String ordersGet(Model model,Authentication authentication){
        List<OrderDto> orderDtos = orderService.createOrderDtoList(authentication.getName());
        model.addAttribute("orders",orderDtos);
        return "orders";
    }

}

