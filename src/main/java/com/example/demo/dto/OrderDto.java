package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderDto {


    private List<ProductDto> productDtoList;
    private String status;
    private String time;

}
