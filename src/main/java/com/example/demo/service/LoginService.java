package com.example.demo.service;

import com.example.demo.dto.LoginDto;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginService {

    @Autowired
    private UserRepository userRepository;

    public boolean login(LoginDto loginDto){
        Optional<User> optionalUser = userRepository.findByEmail(loginDto.getEmail());
        if(optionalUser.isEmpty()){
            return false;
        }
        User user = optionalUser.get();
        if(!user.getPassword().equals(loginDto.getPassword())){
            return false;
        }
        return true;
    }
}
