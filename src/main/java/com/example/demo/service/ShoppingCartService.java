package com.example.demo.service;

import com.example.demo.dto.PriceSummaryDto;
import com.example.demo.dto.ProductDto;
import com.example.demo.dto.QuantityDto;
import com.example.demo.dto.QuantityPriceDto;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Product;
import com.example.demo.model.ShoppingCart;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ShoppingCartService {

    @Autowired
    ShoppingCartRepository shoppingCartRepository;
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    public boolean addProductToShoppingCart(String productId, QuantityDto quantityDto, String email) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(email);
        System.out.println("am gasit cartu " + shoppingCart.getShoppingCartId());

        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        if (optionalProduct.isEmpty()) {
            System.out.println("optional product is empty");
            return false;
        }
        Product product = optionalProduct.get();
        Integer quantity = Integer.valueOf(quantityDto.getQuantity());
        if (product.getStock().getQuantity() < quantity) {
            return false;
        }
        for(int index = 0;index<quantity;index++){
            shoppingCart.addProduct(product);
        }
        shoppingCartRepository.save(shoppingCart);
        return true;
    }

    public Map<ProductDto, QuantityPriceDto> retrieveShoppingCartContent(String userEmail) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(userEmail);
        List<Product> products = shoppingCart.getProducts();
        Map<ProductDto,QuantityPriceDto> result = new LinkedHashMap<>();
        for(Product product : products){
            ProductDto productDto = productMapper.map(product);
            if (result.containsKey(productDto)){
                QuantityPriceDto quantityPriceDto = result.get(productDto);

                Integer numberOfProducts = quantityPriceDto.getQuantity();
                quantityPriceDto.setQuantity(numberOfProducts + 1);

                Integer totalPrice = quantityPriceDto.getTotalPrice();
                quantityPriceDto.setTotalPrice(totalPrice + Integer.parseInt(productDto.getPrice()));
            } else {
                QuantityPriceDto quantityPriceDto = new QuantityPriceDto();
                quantityPriceDto.setTotalPrice(Integer.valueOf(productDto.getPrice()));
                quantityPriceDto.setQuantity(1);
                result.put(productDto,quantityPriceDto);
            }
        }
        System.out.println(result);
        return result;
    }


    public PriceSummaryDto computePriceSummaryDto(Map<ProductDto, QuantityPriceDto> productMap) {
        int subtotalPrice = productMap.values().stream()
                .map(QuantityPriceDto::getTotalPrice)
                .mapToInt(Integer::intValue)
                .sum();
        int shipping = 50;
        int total = subtotalPrice + shipping;
        PriceSummaryDto priceSummaryDto = new PriceSummaryDto(subtotalPrice,shipping,total);
        return priceSummaryDto;
    }


}
