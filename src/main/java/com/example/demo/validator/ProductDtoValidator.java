package com.example.demo.validator;

import com.example.demo.dto.ProductDto;
import com.example.demo.model.enums.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public class ProductDtoValidator {
    public void validate(ProductDto productDto, BindingResult bindingResult) {

        validateName(productDto,bindingResult);
        validatePrice(productDto, bindingResult);
        validateCategory(productDto,bindingResult);

    }

    private void validateName(ProductDto productDto, BindingResult bindingResult) {
        if (productDto.getName().length() ==0) {
            FieldError fieldError = new FieldError("productDto", "name", "Please input a name");
            bindingResult.addError(fieldError);
        }
    }

    private void validatePrice(ProductDto productDto, BindingResult bindingResult) {
        try {
            Integer price = Integer.valueOf(productDto.getPrice());
            if (price <= 0){
                FieldError fieldError = new FieldError("productDto", "price", "Price should be positive");
                bindingResult.addError(fieldError);
            }
        } catch (IllegalArgumentException exception) {
            FieldError fieldError = new FieldError("productDto", "price", "Price should be numerical value");
            bindingResult.addError(fieldError);
        }
    }

    private void validateCategory(ProductDto productDto, BindingResult bindingResult) {
        try {
            Category category = Category.valueOf(productDto.getCategory());
        } catch (IllegalArgumentException exception) {
            FieldError fieldError = new FieldError("productDto", "category", "Category is not valid");
            bindingResult.addError(fieldError);
        }
    }

    public void validateImage(MultipartFile multipartFile,BindingResult bindingResult){
        try {
            byte[] image = multipartFile.getBytes();
            if (image.length > 0) {
                return;
            }


        }catch (Exception exception){

        }
        FieldError fieldError = new FieldError("productDto", "image", "Invalid image");
        bindingResult.addError(fieldError);
    }
}